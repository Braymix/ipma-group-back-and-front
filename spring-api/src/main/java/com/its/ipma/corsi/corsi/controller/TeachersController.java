package com.its.ipma.corsi.corsi.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.its.ipma.corsi.corsi.dao.TeachersDao;
import com.its.ipma.corsi.corsi.dto.BaseResponseDto;
import com.its.ipma.corsi.corsi.dto.TeachersDto;
import com.its.ipma.corsi.corsi.services.interfaces.TeachersInterface;

@RestController
@RequestMapping(value = "api/teachers")
public class TeachersController {
	private static final Logger logger = LoggerFactory.getLogger(TeachersController.class);

	@Autowired
	TeachersInterface teachersService;

	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<TeachersDto>> fetchAll() {

		logger.info("****** FETCH *******");

		BaseResponseDto<List<TeachersDto>> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		List<TeachersDto> teachers = teachersService.fetchAll();

		if (teachers.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT.value());
			response.setMessage("No entry found");
		} else {
			response.setResponse(teachers);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Servizio elaborato correttamente");
		}

		return response;
	}

	@PostMapping(produces = "application/json", value = "filter")
	public BaseResponseDto<List<TeachersDto>> filter(@RequestBody TeachersDao filters) {

		logger.info("****** FILTER *******");

		BaseResponseDto<List<TeachersDto>> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		List<TeachersDto> teachers = teachersService.filter(filters);

		if (teachers.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT.value());
			response.setMessage("No entry found");
		} else {
			response.setResponse(teachers);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Servizio elaborato correttamente");
		}

		return response;
	}

	@PostMapping(produces = "application/json")
	public BaseResponseDto<TeachersDto> createTeacher(@RequestBody TeachersDao teacher) {

		logger.info("****** CREATE *******");
		BaseResponseDto<TeachersDto> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		response.setResponse(teacher.convertToDto());

			if(teacher.getPartitaIva()!=null && teacher.getPartitaIva().length()==11 || teacher.getPartitaIva()==null
					&& teacher.getTelefono()!=null) {
			teachersService.create(teacher);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Teacher added correctly");
			}
			else if(teacher.getPartitaIva()!=null && teacher.getPartitaIva().length()!=11) {
				response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
				response.setMessage("Partita Iva is incorrect");
			}
			else if(teacher.getTelefono()==null) {
				response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
				response.setMessage("Telefono can not be null");
			}
		   else {
			    response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			    response.setMessage("Teacher not added");
		}

		return response;
	}

	@PatchMapping(produces = "application/json")
	public BaseResponseDto<TeachersDto> updateTeacher(@RequestBody TeachersDao teacher) {

		logger.info("****** UPDATE *******");

		BaseResponseDto<TeachersDto> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		response.setResponse(teacher.convertToDto());

		if(teacher.getTelefono()==null) {
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setMessage("Telefono can not be null");
		}	
		else if(teacher.getPartitaIva()!=null && teacher.getPartitaIva().length()==11 || teacher.getPartitaIva()==null) {
		teachersService.update(teacher);
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("Teacher changed correctly");
		}		
		else if(teacher.getPartitaIva()!=null && teacher.getPartitaIva().length()!=11) {
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setMessage("Partita Iva is incorrect");
		}	
	   else {
		    response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		    response.setMessage("Teacher not changed");
	  }

		return response;
	}

	@DeleteMapping(produces = "application/json", path = "/{id}")
	public BaseResponseDto<TeachersDto> deleteById(@PathVariable int id) {

		logger.info("****** DELETE *******");

		BaseResponseDto<TeachersDto> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		teachersService.deleteById(id);
		response.setResponse(id);
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("Teacher deleted correctly");

		return response;
	}
	
	@GetMapping(value = "/{pagina}...{quantita}", produces = "application/json")
	public BaseResponseDto<List<TeachersDto>> fetchPagina(@PathVariable("pagina") int pagina, @PathVariable("quantita") int quantita) {

		logger.info("****** FETCH IMPAGINAZIONE*******");

		BaseResponseDto<List<TeachersDto>> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		List<TeachersDto> teachers;

		teachers = teachersService.pageLayout(pagina, quantita);
		if (teachers.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT.value());
			response.setMessage("No entry found");
		} else {
			response.setResponse(teachers);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Servizio elaborato correttamente");
		}

		return response;
	}

}
