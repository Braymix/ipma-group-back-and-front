package com.its.ipma.corsi.corsi.services.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.its.ipma.corsi.corsi.dao.TeachersDao;
import com.its.ipma.corsi.corsi.dto.TeachersDto;
import com.its.ipma.corsi.corsi.repository.TeachersRepository;
import com.its.ipma.corsi.corsi.services.interfaces.TeachersInterface;

@Service
@Transactional
public class TeachersServiceImpl implements TeachersInterface {

	@Autowired
	TeachersRepository teachersRepository;

	@Override
	public boolean create(TeachersDao dao) {

		try {
			teachersRepository.save(dao);
		} catch (Exception e) {
			return false;
		}
		return true;

	}

	@Override
	public List<TeachersDto> fetchAll() {

		List<TeachersDto> list = new ArrayList<>();
		try {
			for (TeachersDao teacher : teachersRepository.findAll()) {
				list.add(teacher.convertToDto());
			}
		} catch (Exception e) {
			return null;
		}
		return list;
	}

	@Override
	public boolean update(TeachersDao dao) {
		if (teachersRepository.existsById(dao.getDocenteId())) {
			try {
				teachersRepository.save(dao);
			} catch (Exception e) {
				return false;
			}
			return true;
		}
		return false;

	}

	@Override
	public boolean deleteById(int id) {

		if (teachersRepository.existsById(id)) {
			teachersRepository.deleteById(id);
			return true;
		} else {
			return false;
		}

	}

	@Override
	public TeachersDto readById(int id) {

		try {
			return teachersRepository.getOne(id).convertToDto();
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public List<TeachersDto> filter(TeachersDao filters) {

		ExampleMatcher caseInsensitiveExampleMatcher = ExampleMatcher.matchingAll().withIgnoreCase()
				.withMatcher("titolo", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("ragioneSociale", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("indirizzo", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("localita", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("provincia", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("nazione", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("telefono", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("fax", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("partitaIva", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("referente", ExampleMatcher.GenericPropertyMatchers.contains());

		Example<TeachersDao> example = Example.of(filters, caseInsensitiveExampleMatcher);

		List<TeachersDao> teachersDao = teachersRepository.findAll(example);
		ArrayList<TeachersDto> teachersDto = new ArrayList<TeachersDto>();
		for (TeachersDao teacher : teachersDao) {
			teachersDto.add(teacher.convertToDto());
		}
		return teachersDto;
	}

	@Override
	public List<TeachersDto> pageLayout(int pagina, int quantita) {
		Pageable p = PageRequest.of(pagina-1, quantita);
		Page<TeachersDao> listaPagina = teachersRepository.findAll(p);
		ArrayList<TeachersDto> teachersDto = new ArrayList<TeachersDto>();
		for(TeachersDao t : listaPagina) {
			teachersDto.add(t.convertToDto());
		}
		return teachersDto;
	}

}
