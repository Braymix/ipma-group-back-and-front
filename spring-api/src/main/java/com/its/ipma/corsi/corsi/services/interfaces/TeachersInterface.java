package com.its.ipma.corsi.corsi.services.interfaces;

import java.util.List;

import com.its.ipma.corsi.corsi.dao.TeachersDao;
import com.its.ipma.corsi.corsi.dto.TeachersDto;

public interface TeachersInterface {
	TeachersDto readById(int id);

	List<TeachersDto> filter(TeachersDao filters);

	List<TeachersDto> fetchAll();
	
	List<TeachersDto> pageLayout(int pagina, int quantita);

	boolean update(TeachersDao dao);

	boolean deleteById(int id);

	boolean create(TeachersDao dao);
}
