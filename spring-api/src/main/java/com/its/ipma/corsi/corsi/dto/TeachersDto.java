package com.its.ipma.corsi.corsi.dto;

import com.its.ipma.corsi.corsi.dao.TeachersDao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TeachersDto {

	private Integer docenteId;
	private String titolo;
	private String ragioneSociale;
	private String indirizzo;
	private String localita;
	private String provincia;
	private String nazione;
	private String telefono;
	private String fax;
	private String partitaIva;
	private String referente;

	public TeachersDao convertToDao() {

		TeachersDao dao = new TeachersDao();

		dao.setDocenteId(this.getDocenteId());
		dao.setFax(this.getFax());
		dao.setIndirizzo(this.getIndirizzo());
		dao.setLocalita(this.getLocalita());
		dao.setNazione(this.getNazione());
		dao.setPartitaIva(this.getPartitaIva());
		dao.setProvincia(this.getProvincia());
		dao.setRagioneSociale(this.getRagioneSociale());
		dao.setReferente(this.getReferente());
		dao.setTelefono(this.getTelefono());
		dao.setTitolo(this.getTitolo());

		return dao;

	}

}
