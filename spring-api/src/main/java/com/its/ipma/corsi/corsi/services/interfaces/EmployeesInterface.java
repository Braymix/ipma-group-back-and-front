package com.its.ipma.corsi.corsi.services.interfaces;

import java.util.List;

import com.its.ipma.corsi.corsi.dao.EmployeesDao;
import com.its.ipma.corsi.corsi.dto.EmployeesDto;

public interface EmployeesInterface {
	EmployeesDto readById(int id);

	List<EmployeesDto> filter(EmployeesDao filters);

	List<EmployeesDto> fetchAll();
	
	List<EmployeesDto> pageLayout(int pagina, int quantita);

}
