package com.its.ipma.corsi.corsi.services.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.its.ipma.corsi.corsi.dao.EmployeesDao;
import com.its.ipma.corsi.corsi.dao.TeachersDao;
import com.its.ipma.corsi.corsi.dto.EmployeesDto;
import com.its.ipma.corsi.corsi.dto.TeachersDto;
import com.its.ipma.corsi.corsi.repository.EmployeesRepository;
import com.its.ipma.corsi.corsi.services.interfaces.EmployeesInterface;

@Service
@Transactional
public class EmployeesServiceImpl implements EmployeesInterface {

	@Autowired
	EmployeesRepository employeesRepository;

	@Override
	public List<EmployeesDto> fetchAll() {

		List<EmployeesDto> list = new ArrayList<>();
		try {
			for (EmployeesDao employees : employeesRepository.findAll()) {
				list.add(employees.convertToDto());
			}
		} catch (Exception e) {
			return null;
		}
		return list;
	}

	@Override
	public List<EmployeesDto> filter(EmployeesDao filters) {

		ExampleMatcher caseInsensitiveExampleMatcher = ExampleMatcher.matchingAll().withIgnoreCase()
				.withMatcher("matricola", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("societa.societaId", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("societa.ragioneSociale", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("societa.indirizzo", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("societa.localita", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("societa.provincia", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("societa.nazione", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("societa.telefono", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("societa.fax", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("societa.partitaIva", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("cognome", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("nome", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("sesso", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("dataNascita", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("luogoNascita", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("statoCivile", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("titoloStudio", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("conseguitoPresso", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("annoConseguimento", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("tipoDipendente", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("qualifica", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("livello", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("dataAssunzione", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("responsabileRisorsa", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("responsabileArea", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("dataFineRapporto", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("dataScadenzaContratto", ExampleMatcher.GenericPropertyMatchers.contains());

		Example<EmployeesDao> example = Example.of(filters, caseInsensitiveExampleMatcher);

		List<EmployeesDao> employeesDao = employeesRepository.findAll(example);
		ArrayList<EmployeesDto> employeesDto = new ArrayList<EmployeesDto>();
		for (EmployeesDao employee : employeesDao) {
			employeesDto.add(employee.convertToDto());
		}
		return employeesDto;
	}

	@Override
	public EmployeesDto readById(int id) {

		try {
			return employeesRepository.getOne(id).convertToDto();
		} catch (Exception e) {
			return null;
		}

	}


	@Override
	public List<EmployeesDto> pageLayout(int pagina, int quantita) {
		Pageable p = PageRequest.of(pagina-1, quantita);
		Page<EmployeesDao> listaPagina = employeesRepository.findAll(p);
		ArrayList<EmployeesDto> employeesDto = new ArrayList<EmployeesDto>();
		for(EmployeesDao e : listaPagina) {
			employeesDto.add(e.convertToDto());
		}
		return employeesDto;
	}

}
