package com.its.ipma.corsi.corsi.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.its.ipma.corsi.corsi.dao.CorsiDao;
import com.its.ipma.corsi.corsi.dto.BaseResponseDto;
import com.its.ipma.corsi.corsi.dto.CorsiDto;
import com.its.ipma.corsi.corsi.services.interfaces.CorsiInterface;

@RestController
@RequestMapping(value = "api/corsi")
public class CorsiController {
	private static final Logger logger = LoggerFactory.getLogger(CorsiController.class);

	@Autowired
	CorsiInterface corsiService;

	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<CorsiDto>> fetchAll() {

		logger.info("****** FETCH *******");

		BaseResponseDto<List<CorsiDto>> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		List<CorsiDto> corsi = corsiService.fetchAll();

		if (corsi.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT.value());
			response.setMessage("No entry found");
		} else {
			response.setResponse(corsi);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Servizio elaborato correttamente");
		}

		return response;
	}

	@PostMapping(produces = "application/json", value = "filter")
	public BaseResponseDto<List<CorsiDto>> filter(@RequestBody CorsiDao filters) {

		logger.info("****** FILTER *******");

		BaseResponseDto<List<CorsiDto>> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		List<CorsiDto> corsi = corsiService.filter(filters);

		if (corsi.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT.value());
			response.setMessage("No entry found");
		} else {
			response.setResponse(corsi);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Servizio elaborato correttamente");
		}

		return response;
	}

	@GetMapping(value = "/{pagina}...{quantita}", produces = "application/json")
	public BaseResponseDto<List<CorsiDto>> fetchPagina(@PathVariable("pagina") int pagina,
			@PathVariable("quantita") int quantita) {

		logger.info("****** FETCH IMPAGINAZIONE*******");

		BaseResponseDto<List<CorsiDto>> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		List<CorsiDto> employees;

		employees = corsiService.pageLayout(pagina, quantita);
		if (employees.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT.value());
			response.setMessage("No entry found");
		} else {
			response.setResponse(employees);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Servizio elaborato correttamente");
		}

		return response;
	}

	@PostMapping(produces = "application/json")
	public BaseResponseDto<CorsiDto> createTeacher(@RequestBody CorsiDao corso) {

		logger.info("****** CREATE *******");
		BaseResponseDto<CorsiDto> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		response.setResponse(corso.convertToDto());

		if (corso.getTipo() != null && corso.getTipologia() != null && corso.getDescrizione() != null
				&& corso.getEdizione() != null && corso.getPrevisto() != null && corso.getErogato() != null
				&& corso.getDurata() != null && corso.getMisura() != null) {
			corsiService.create(corso);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Corso added correctly");
		} else if (corso.getTipo() == null || corso.getTipologia() == null || corso.getDescrizione() == null
				|| corso.getEdizione() == null || corso.getPrevisto() == null || corso.getErogato() == null
				|| corso.getDurata() == null || corso.getMisura() == null) {
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setMessage("First 8 parameters can not be null");
		} else {
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setMessage("Corso not added");
		}

		return response;
	}

	@PatchMapping(produces = "application/json")
	public BaseResponseDto<CorsiDto> updateTeacher(@RequestBody CorsiDao corsi) {

		logger.info("****** UPDATE *******");

		BaseResponseDto<CorsiDto> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		response.setResponse(corsi.convertToDto());

		if (corsiService.update(corsi)) {
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Corso changed correctly");
		} else {
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setMessage("Corso not changed");
		}

		return response;
	}

	@DeleteMapping(produces = "application/json", path = "/{id}")
	public BaseResponseDto<CorsiDto> deleteById(@PathVariable int id) {

		logger.info("****** DELETE *******");

		BaseResponseDto<CorsiDto> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		corsiService.deleteById(id);
		response.setResponse(id);
		response.setStatus(HttpStatus.OK.value());
		response.setMessage("Corso deleted correctly");

		return response;
	}

}
