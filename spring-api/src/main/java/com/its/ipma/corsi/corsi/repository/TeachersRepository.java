package com.its.ipma.corsi.corsi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.its.ipma.corsi.corsi.dao.TeachersDao;

public interface TeachersRepository extends JpaRepository<TeachersDao, Integer> {
}
