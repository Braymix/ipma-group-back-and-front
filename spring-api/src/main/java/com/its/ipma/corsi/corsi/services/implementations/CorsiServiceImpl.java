package com.its.ipma.corsi.corsi.services.implementations;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.its.ipma.corsi.corsi.dao.CorsiDao;
import com.its.ipma.corsi.corsi.dto.CorsiDto;
import com.its.ipma.corsi.corsi.repository.CorsiRepository;
import com.its.ipma.corsi.corsi.services.interfaces.CorsiInterface;

@Service
@Transactional
public class CorsiServiceImpl implements CorsiInterface {

	@Autowired
	CorsiRepository corsiRepository;

	@Override
	public boolean create(CorsiDao dao) {

		try {
			corsiRepository.save(dao);
		} catch (Exception e) {
			return false;
		}
		return true;

	}

	@Override
	public List<CorsiDto> fetchAll() {

		List<CorsiDto> list = new ArrayList<>();
		try {
			for (CorsiDao corsi : corsiRepository.findAll()) {
				list.add(corsi.convertToDto());
			}
		} catch (Exception e) {
			return null;
		}
		return list;
	}

	@Override
	public boolean update(CorsiDao dao) {
		if (corsiRepository.existsById(dao.getCorsoId())) {
			try {
				corsiRepository.save(dao);
			} catch (Exception e) {
				return false;
			}
			return true;
		}
		return false;

	}

	@Override
	public boolean deleteById(int id) {

		if (corsiRepository.existsById(id)) {
			corsiRepository.deleteById(id);
			return true;
		} else {
			return false;
		}

	}

	@Override
	public CorsiDto readById(int id) {

		try {
			return corsiRepository.getOne(id).convertToDto();
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public List<CorsiDto> filter(CorsiDao filters) {

		ExampleMatcher caseInsensitiveExampleMatcher = ExampleMatcher.matchingAll().withIgnoreCase()
				.withMatcher("corsoId", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("tipo.descrizione", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("tipologia.descrizione", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("descrizione", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("edizione", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("previsto", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("erogato", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("durata", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("misura.descrizione", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("note", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("luogo", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("ente", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("dataErogazione", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("dataChiusura", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("dataCensimento", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("interno", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("tipoId", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("tipologiaId", ExampleMatcher.GenericPropertyMatchers.contains())
				.withMatcher("misuraId", ExampleMatcher.GenericPropertyMatchers.contains());

		Example<CorsiDao> example = Example.of(filters, caseInsensitiveExampleMatcher);

		List<CorsiDao> corsiDao = corsiRepository.findAll(example);
		ArrayList<CorsiDto> corsiDto = new ArrayList<CorsiDto>();
		for (CorsiDao corsi : corsiDao) {
			corsiDto.add(corsi.convertToDto());
		}
		return corsiDto;
	}

	@Override
	public List<CorsiDto> pageLayout(int pagina, int quantita) {
		Pageable p = PageRequest.of(pagina - 1, quantita);
		Page<CorsiDao> listaPagina = corsiRepository.findAll(p);
		ArrayList<CorsiDto> employeesDto = new ArrayList<CorsiDto>();
		for (CorsiDao e : listaPagina) {
			employeesDto.add(e.convertToDto());
		}
		return employeesDto;
	}

}
