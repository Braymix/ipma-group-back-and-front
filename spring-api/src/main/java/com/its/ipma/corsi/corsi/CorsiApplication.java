package com.its.ipma.corsi.corsi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CorsiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CorsiApplication.class, args);
	}

}
