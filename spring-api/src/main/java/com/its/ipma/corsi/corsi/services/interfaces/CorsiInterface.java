package com.its.ipma.corsi.corsi.services.interfaces;

import java.util.List;

import com.its.ipma.corsi.corsi.dao.CorsiDao;
import com.its.ipma.corsi.corsi.dto.CorsiDto;

public interface CorsiInterface {
	CorsiDto readById(int id);

	List<CorsiDto> filter(CorsiDao filters);

	List<CorsiDto> fetchAll();

	boolean update(CorsiDao dao);

	boolean deleteById(int id);

	boolean create(CorsiDao dao);

	List<CorsiDto> pageLayout(int pagina, int quantita);
}
