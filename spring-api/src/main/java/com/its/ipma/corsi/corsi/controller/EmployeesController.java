package com.its.ipma.corsi.corsi.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;

import com.its.ipma.corsi.corsi.dao.EmployeesDao;
import com.its.ipma.corsi.corsi.dto.BaseResponseDto;
import com.its.ipma.corsi.corsi.dto.EmployeesDto;
import com.its.ipma.corsi.corsi.services.interfaces.EmployeesInterface;

@RestController
@RequestMapping(value = "api/employees")
public class EmployeesController {
	private static final Logger logger = LoggerFactory.getLogger(EmployeesController.class);

	@Autowired
	EmployeesInterface employeesService;

	@GetMapping(produces = "application/json")
	public BaseResponseDto<List<EmployeesDto>> fetchAll() {

		logger.info("****** FETCH *******");

		BaseResponseDto<List<EmployeesDto>> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		List<EmployeesDto> employees = employeesService.fetchAll();

		if (employees.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT.value());
			response.setMessage("No entry found");
		} else {
			response.setResponse(employees);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Servizio elaborato correttamente");
		}

		return response;
	}

	@PostMapping(produces = "application/json", value = "filter")
	public BaseResponseDto<List<EmployeesDto>> filter(@RequestBody EmployeesDao filters) {

		logger.info("****** FILTER *******");

		BaseResponseDto<List<EmployeesDto>> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		List<EmployeesDto> employees = employeesService.filter(filters);

		if (employees.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT.value());
			response.setMessage("No entry found");
		} else {
			response.setResponse(employees);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Servizio elaborato correttamente");
		}

		return response;
	}

	@GetMapping(value = "/{pagina}...{quantita}", produces = "application/json")
	public BaseResponseDto<List<EmployeesDto>> fetchPagina(@PathVariable("pagina") int pagina, @PathVariable("quantita") int quantita) {

		logger.info("****** FETCH IMPAGINAZIONE*******");

		BaseResponseDto<List<EmployeesDto>> response = new BaseResponseDto<>();
		response.setTimestamp(new Date());

		List<EmployeesDto> employees;

		employees = employeesService.pageLayout(pagina, quantita);
		if (employees.isEmpty()) {
			response.setStatus(HttpStatus.NO_CONTENT.value());
			response.setMessage("No entry found");
		} else {
			response.setResponse(employees);
			response.setStatus(HttpStatus.OK.value());
			response.setMessage("Servizio elaborato correttamente");
		}

		return response;
	}
}
