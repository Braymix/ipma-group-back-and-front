http://localhost:8090/api/teachers
GET: Ritorna tutti i professori

POST: Riceve un TeacherDao nel body e lo inserisce
        ID non necessario.

PATCH: Riceve un TeacherDao nel body e modifica l'insegnante con l'id ricevuto inserendo i dati del resto del DAO se presenti
            Se l'ID non è presente nel database non fa nulla
            Se un parametro non è presente rimane uguale

DELETE: Riceve un intero nel body ed elimina l'insegnate con l'ID ricevuto se presente

http://localhost:8090/api/teachers/filter
POST: Riceve un TeacherDao nel body ritorna una lista filtrata in base ai parametri ricevuti nel TeacherDao

http://localhost:8090/api/employees
GET: Ritorna tutti i dipendenti

http://localhost:8090/api/employees/filter
POST: Riceve un EmployeesDao nel body ritorna una lista filtrata in base ai parametri ricevuti nel EmployeesDao

http://localhost:8090/api/corsi
GET: Ritorna tutti i corsi

POST: Riceve un CorsiDao nel body e lo inserisce
        ID non necessario.

PATCH: Riceve un CorsiDao nel body e modifica il corso con l'id ricevuto inserendo i dati del resto del DAO se presenti
            Se l'ID non è presente nel database non fa nulla
            Se un parametro non è presente rimane uguale

DELETE: Riceve un intero nel body ed elimina il corso con l'ID ricevuto se presente

http://localhost:8090/api/corsi/filter
POST: Riceve un CorsiDao nel body ritorna una lista filtrata in base ai parametri ricevuti nel CorsiDao

http://localhost:8090/api/employees/1...4
GET: Impaginazione dipendenti
        Riceve come primo parametro il numero della pagina in cui ci si trova e come secondo parametro quanti dipendenti mostrare in ogni singola pagina 
