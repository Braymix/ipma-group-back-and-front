import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CorsiRoutingModule } from './corsi-routing.module';
import { CorsiComponent } from './corsi.component';

import { HttpClientModule } from "@angular/common/http";
import { HttpModule } from "@angular/http";
import { SidebarModule } from "primeng/sidebar";
import { TableModule } from "primeng/table";
import { DialogModule, Dialog } from "primeng/dialog";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ButtonModule } from "primeng/button";
import { MessagesModule } from "primeng/messages";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DropdownModule } from "primeng/dropdown";
import { paix } from "paix";


@NgModule({
  declarations: [CorsiComponent],
  imports: [
    CommonModule,
    CorsiRoutingModule,
    HttpClientModule,
    HttpModule,
    DialogModule,
    Dialog,
    SidebarModule,
    TableModule,
    ConfirmDialogModule,
    ButtonModule,
    MessagesModule,
    FormsModule,
    ReactiveFormsModule,
    DropdownModule,
    paix
  ]
})
export class CorsiModule { }
