import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Message, ConfirmationService } from "primeng/api";
import { ApiService } from '../../../core/services/api.service';
import { CourseService } from '../../../core/services/course.service';
import { Router } from '@angular/router';
import { paix } from "paix";

@Component({
  selector: 'app-corsi',
  templateUrl: './corsi.component.html',
  styleUrls: ['./corsi.component.css'],
  providers: [ConfirmationService]
})
export class CorsiComponent implements OnInit {

  public filtroSelezionato: any;

  public tipo: any[] = [];

  public pagina: any = 1;

  public dati: any = 4;

  public nPagine: any = 0;

  public formGroup: FormGroup;

  public visibleSidebar5: boolean;

  msgs: Message[] = [];

  public utCorr: any;

  public lista: any[] = [];
  public lista2: any[] = [];

  constructor(
    public api: ApiService,
    public corsiService: CourseService,
    public router: Router,
    private confirmationService: ConfirmationService,
    public fb: FormBuilder
  ) {
    this.tipo = [
      { name: "id", code: "corsoId" },
      { name: "description", code: "descrizione" },
      { name: "edition", code: "edizione" },
      { name: "expected", code: "previsto" },
      { name: "provided", code: "erogato" },
      { name: "duration", code: "durata" },
      { name: "notes", code: "note" },
      { name: "location", code: "luogo" },
      { name: "enity", code: "ente" },
      { name: "delivery date", code: "dataErogazione" },
      { name: "closure date", code: "dataChiusura" },
      { name: "census date", code: "dataCensimento" },
      { name: "internal", code: "interno" },
    ];
  }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      soggFiltrato: [""],
    });

    this.corsiService.getAll().subscribe((resp) => {
      this.lista2 = resp.response;
    });

    this.loadAll();
  }

  confirm1(val: any) {
    this.confirmationService.confirm({
      message: "Sei sicuro di voler eliminare il corso?",
      header: "Conferma",
      icon: "pi pi-exclamation-triangle",
      key: "conferma",
      accept: () => {
        this.msgs = [
          {
            severity: "success",
            summary: "Confermato",
            detail: "Eliminazione Completata",
          },
        ];
        this.delete(val);
        this.confirmationService.close();
      },
      reject: () => {
        this.msgs = [
          {
            severity: "info",
            summary: "Annullato",
            detail: "Operazione Annullata",
          },
        ];
        this.confirmationService.close();
      },
    });
  }

  loadAll() {
    this.corsiService
      .getAllPage(this.pagina, this.dati)
      .subscribe((resp) => {
        this.lista = resp.response;
      });
  }

  show(valore: any) {
    this.visibleSidebar5 = true;

    this.utCorr = valore;
  }

  new() {
    this.router.navigate(["/newCourse"]);
  }

  delete(val: any) {
    this.corsiService.deleteById(val).subscribe(() => {
      if (this.filtroSelezionato == null){
        this.loadAll();
        }
        else{
          this.filtra();
        }
    });
  }

  edit(id: any) {
    this.router.navigate(["/editCourse/" + id]);
  }

  filtra() {
    let res = { nome: this.formGroup.value.soggFiltrato };
    let replacement = { nome: this.filtroSelezionato.code };
    let modificato = paix(res, replacement);

    this.corsiService.getById(modificato).subscribe((resp) => {
      this.lista = resp.response;
      this.lista2 = resp.response;
    });
  }

  avanti() {
    this.nPagine = this.lista2.length / this.dati;

    if (this.pagina < this.nPagine) this.pagina++;

    if (this.filtroSelezionato == null){
    this.loadAll();
    }
    else{
      this.filtra();
    }
  }

  dietro() {
    if (this.pagina - 1 > 0) {
      this.pagina--;

      if (this.filtroSelezionato == null){
        this.loadAll();
        }
        else{
          this.filtra();
        }
    }
  }
  removeFilter() {
    this.loadAll();
    this.filtroSelezionato = null;
    this.formGroup = this.fb.group({
      soggFiltrato: [""],
    });
  }

}
