import { Component, OnInit } from "@angular/core";
import { ApiService } from "../../../core/services/api.service";
import { EmployeesServices } from "../../../core/services/employees.services";
import { FormGroup, FormBuilder } from "@angular/forms";
import { paix } from "paix";

@Component({
  selector: "app-employees",
  templateUrl: "./employees.component.html",
  styleUrls: ["./employees.component.css"],
})
export class EmployeesComponent implements OnInit {
  public pagina: any = 1;

  public filtroSelezionato: any;

  public tipo2: any[] = [];

  public dati: any = 4;

  public nPagine: any = 0;

  public formGroup: FormGroup;

  public lista: any[] = [];

  public lista2: any[] = [];

  public visibleSidebar5: boolean;

  public utCorr: any;

  constructor(
    public api: ApiService,
    public employeesService: EmployeesServices,
    public fb: FormBuilder
  ) {
    this.tipo2 = [
      { name: "employees Id", code: "dipendenteId" },
      { name: "freshman", code: "matricola" },
      { name: "surname", code: "cognome" },
      { name: "first name", code: "nome" },
      { name: "gender", code: "sesso" },
      { name: "date of birth", code: "dataNascita" },
      { name: "birth place", code: "luogoNascita" },
      { name: "marital status", code: "statoCivile" },
      { name: "educational qualification", code: "titoloStudio" },
      { name: "achieved at", code: "conseguitoPresso" },
      { name: "Year Graduated", code: "annoConseguimento" },
      { name: "Employee type", code: "tipoDipendente" },
      { name: "qualification", code: "qualifica" },
      { name: "level", code: "livello" },
      { name: "assumption date", code: "dataAssunzione" },
      { name: "area manager", code: "responsabileRisorsa" },
      { name: "Resource manager", code: "responsabileArea" },
      { name: "End of relationship date", code: "dataFineRapporto" },
      { name: "Contract Expiration date", code: "dataScadenzaContratto" },
    ];
  }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      soggFiltrato: [""],
    });

    this.employeesService.getAll().subscribe((resp) => {
      this.lista2 = resp.response;
    });

    this.loadAll();
  }

  loadAll() {
    this.employeesService
      .getAllPage(this.pagina, this.dati)
      .subscribe((resp) => {
        this.lista = resp.response;
      });
  }

  show(valore: any) {
    this.visibleSidebar5 = true;

    this.utCorr = valore;
  }

  filtra() {
    let res = { nome: this.formGroup.value.soggFiltrato };
    let replacement = { nome: this.filtroSelezionato.code };
    let modificato = paix(res, replacement);

    this.employeesService.getById(modificato).subscribe((resp) => {
      this.lista = resp.response;
      this.lista2 = resp.response;
    });
  }

  avanti() {
    this.nPagine = this.lista2.length / this.dati;

    if (this.pagina < this.nPagine) this.pagina++;

    if (this.filtroSelezionato == null) {
      this.loadAll();
    } else {
      this.filtra();
    }
  }

  dietro() {
    if (this.pagina - 1 > 0) {
      this.pagina--;

      if (this.filtroSelezionato == null) {
        this.loadAll();
      } else {
        this.filtra();
      }
    }
  }

  removeFilter() {
    this.loadAll();
    this.filtroSelezionato = null;
    this.formGroup = this.fb.group({
      soggFiltrato: [""],
    });
  }
}
