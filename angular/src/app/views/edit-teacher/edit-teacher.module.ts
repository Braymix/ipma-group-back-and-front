import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditTeacherRoutingModule } from './edit-teacher-routing.module';
import { EditTeacherComponent } from './edit-teacher/edit-teacher.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [EditTeacherComponent],
  imports: [
    CommonModule,
    EditTeacherRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class EditTeacherModule { }
