import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TeachersService } from '../../../core/services/teachers.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-edit-teacher',
  templateUrl: './edit-teacher.component.html',
  styleUrls: ['./edit-teacher.component.css']
})
export class EditTeacherComponent implements OnInit {
  public formGroup: FormGroup;

  constructor(public fb: FormBuilder, public teachersService: TeachersService, public router: Router, public route: ActivatedRoute) { }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      titolo: [''],
      ragioneSociale: [''],
      indirizzo: [''],
      localita: [''],
      provincia: [''],
      nazione: [''],
      telefono: [''],
      fax: [''],
      partitaIva: [''],
      referente: ['']
    });

    this.teachersService.getById("docenteId=" + this.route.snapshot.params['id']).subscribe(resp => {
      this.formGroup = this.fb.group({
        docenteId: [this.route.snapshot.params['id']],
        titolo: [resp.response[0].titolo],
        ragioneSociale: [resp.response[0].ragioneSociale],
        indirizzo: [resp.response[0].indirizzo],
        localita: [resp.response[0].localita],
        provincia: [resp.response[0].provincia],
        nazione: [resp.response[0].nazione],
        telefono: [resp.response[0].telefono],
        fax: [resp.response[0].fax],
        partitaIva: [resp.response[0].partitaIva],
        referente: [resp.response[0].referente]
      });
    });
  }

  conferma(): void {
    this.teachersService.update(this.formGroup.value).subscribe(() => {
      this.router.navigate(['/teachers']);
    });
  }

}
