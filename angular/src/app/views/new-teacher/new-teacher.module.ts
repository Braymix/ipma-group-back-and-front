import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewTeacherRoutingModule } from './new-teacher-routing.module';
import { NewTeacherComponent } from './new-teacher/new-teacher.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [NewTeacherComponent],
  imports: [
    CommonModule,
    NewTeacherRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class NewTeacherModule { }
