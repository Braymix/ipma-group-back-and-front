import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TeachersService } from '../../../core/services/teachers.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-new-teacher',
  templateUrl: './new-teacher.component.html',
  styleUrls: ['./new-teacher.component.css']
})
export class NewTeacherComponent implements OnInit {
  public formGroup: FormGroup;

  constructor(public fb: FormBuilder, public teachersService: TeachersService, public router: Router) { }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      titolo: [''],
      ragioneSociale: [''],
      indirizzo: [''],
      localita: [''],
      provincia: [''],
      nazione: [''],
      telefono: [''],
      fax: [''],
      partitaIva: [''],
      referente: ['']
    });
  }

  conferma(): void {
    this.teachersService.add(this.formGroup.value).subscribe(() => {
      this.router.navigate(['/teachers']);
    });
  }

}
