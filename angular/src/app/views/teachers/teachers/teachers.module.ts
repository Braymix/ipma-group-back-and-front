import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { TeachersRoutingModule } from "./teachers-routing.module";
import { TeachersComponent } from "./teachers.component";

import { HttpClientModule } from "@angular/common/http";
import { HttpModule } from "@angular/http";
import { SidebarModule } from "primeng/sidebar";
import { TableModule } from "primeng/table";
import { DialogModule, Dialog } from "primeng/dialog";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ButtonModule } from "primeng/button";
import { MessagesModule } from "primeng/messages";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DropdownModule } from "primeng/dropdown";
import { paix } from "paix";

@NgModule({
  declarations: [TeachersComponent],
  imports: [
    CommonModule,
    TeachersRoutingModule,
    HttpClientModule,
    HttpModule,
    DialogModule,
    Dialog,
    SidebarModule,
    TableModule,
    ConfirmDialogModule,
    ButtonModule,
    MessagesModule,
    FormsModule,
    ReactiveFormsModule,
    DropdownModule,
    paix
  ],
})
export class TeachersModule {}
