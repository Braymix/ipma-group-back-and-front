import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { TeachersService } from "../../../core/services/teachers.service";
import { ApiService } from "../../../core/services/api.service";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup } from "@angular/forms";
import { paix } from "paix";

import { ConfirmationService } from "primeng/api";
import { Message } from "primeng/api";

@Component({
  selector: "app-teachers",
  templateUrl: "./teachers.component.html",
  styleUrls: ["./teachers.component.css"],
  providers: [ConfirmationService],
})
export class TeachersComponent implements OnInit {
  public filtroSelezionato: any;

  public tipo: any[] = [];

  public pagina: any = 1;

  public dati: any = 4;

  public nPagine: any = 0;

  public formGroup: FormGroup;

  public visibleSidebar5: boolean;

  msgs: Message[] = [];

  public utCorr: any;

  public lista: any[] = [];
  public lista2: any[] = [];

  constructor(
    public api: ApiService,
    public teachersService: TeachersService,
    public router: Router,
    private confirmationService: ConfirmationService,
    public fb: FormBuilder
  ) {
    this.tipo = [
      { name: "id", code: "docenteId" },
      { name: "title", code: "titolo" },
      { name: "business name", code: "ragioneSociale" },
      { name: "address", code: "indirizzo" },
      { name: "resort", code: "localita" },
      { name: "province", code: "provincia" },
      { name: "nation", code: "nazione" },
      { name: "phone number", code: "telefono" },
      { name: "fax", code: "fax" },
      { name: "partita iva", code: "partitaIva" },
      { name: "referent", code: "referente" },
    ];
  }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      soggFiltrato: [""],
    });

    this.teachersService.getAll().subscribe((resp) => {
      this.lista2 = resp.response;
    });

    this.loadAll();
  }
  confirm1(val: any) {
    this.confirmationService.confirm({
      message: "Sei sicuro di voler eliminare il docente?",
      header: "Conferma",
      icon: "pi pi-exclamation-triangle",
      key: "conferma",
      accept: () => {
        this.msgs = [
          {
            severity: "success",
            summary: "Confermato",
            detail: "Eliminazione Completata",
          },
        ];
        this.delete(val);
        this.confirmationService.close();
      },
      reject: () => {
        this.msgs = [
          {
            severity: "info",
            summary: "Annullato",
            detail: "Operazione Annullata",
          },
        ];
        this.confirmationService.close();
      },
    });
  }

  loadAll() {
    this.teachersService
      .getAllPage(this.pagina, this.dati)
      .subscribe((resp) => {
        this.lista = resp.response;
      });
  }

  show(valore: any) {
    this.visibleSidebar5 = true;

    this.utCorr = valore;
  }

  new() {
    this.router.navigate(["/new"]);
  }

  delete(val: any) {
    this.teachersService.deleteById(val).subscribe(() => {
      if (this.filtroSelezionato == null){
        this.loadAll();
        }
        else{
          this.filtra();
        }
    });
  }

  edit(id: any) {
    this.router.navigate(["/edit/" + id]);
  }

  filtra() {
    let res = { nome: this.formGroup.value.soggFiltrato };
    let replacement = { nome: this.filtroSelezionato.code };
    let modificato = paix(res, replacement);

    this.teachersService.getById(modificato).subscribe((resp) => {
      this.lista = resp.response;
      this.lista2 = resp.response;
    });
  }

  avanti() {
    this.nPagine = this.lista2.length / this.dati;

    if (this.pagina < this.nPagine) this.pagina++;

    if (this.filtroSelezionato == null){
    this.loadAll();
    }
    else{
      this.filtra();
    }
  }

  dietro() {
    if (this.pagina - 1 > 0) {
      this.pagina--;

      if (this.filtroSelezionato == null){
        this.loadAll();
        }
        else{
          this.filtra();
        }
    }
  }
  removeFilter() {
    this.loadAll();
    this.filtroSelezionato = null;
    this.formGroup = this.fb.group({
      soggFiltrato: [""],
    });
  }
}
