import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TeachersRoutingModule } from "./teachers-routing.module";
import { TeachersComponent } from "./teachers/teachers.component";
import {TableModule} from 'primeng/table';
import { SidebarModule } from "primeng/sidebar";
import {DropdownModule} from 'primeng/dropdown';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { HttpClientModule } from "@angular/common/http";
import { HttpModule } from "@angular/http";
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ButtonModule} from 'primeng/button';
import {MessagesModule} from 'primeng/messages';




@NgModule({
  declarations: [TeachersComponent],
  imports: [
    CommonModule,
    TeachersRoutingModule,
    TableModule,
    DropdownModule,
    SidebarModule,
    FormsModule,
    ReactiveFormsModule,

    HttpClientModule,
    HttpModule,
    ConfirmDialogModule,
    ButtonModule,
    MessagesModule
  ],
})
export class TeachersModule {}
