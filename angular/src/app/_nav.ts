import { INavData } from "@coreui/angular";

export const navItems: INavData[] = [
  {
    name: "Dashboard",
    url: "/dashboard",
    icon: "icon-speedometer",
    children: [
      {
        name: "teachers",
        url: "/teachers",
        icon: "icon-puzzle",
      },
      {
        name: "home",
        url: "/dashboard",
        icon: "icon-puzzle",
      },
      {
        name: "employees",
        url: "/employees",
        icon: "icon-puzzle",
      },
    ],
  },
];
