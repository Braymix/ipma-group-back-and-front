import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";
import { Observable } from "rxjs";
import { environment } from "../../shared/enviroment";

@Injectable({
  providedIn: "root",
})
export class TeachersService {
  private readonly path = environment.endpoint.teachersController;
  private readonly pathFilter = environment.endpoint.teachersControllerFilter;
  public listSoggetti: any[] = [];

  constructor(private api: ApiService) {}

  
  public getAll(): Observable<any> {
    return this.api.get(this.path);
  }
  public getAllPage(primo:any , secondo:any): Observable<any> {
    return this.api.getByPage(this.path , primo , secondo);
  }
  public getById(ut: any): Observable<any> {
    return this.api.getById(this.pathFilter , ut);
  }
  public add(item: any): Observable<any> {
    return this.api.post(this.path, item);
  }
  public deleteById(body: any): Observable<any> {
    return this.api.delete(this.path, body);
  }
  public update(soggetto: any): Observable<any> {
    return this.api.patch(this.path, soggetto);
  }
}
