import { Injectable } from "@angular/core";
import { ApiService } from "./api.service";
import { Observable } from "rxjs";
import { environment } from "../../shared/enviroment";

@Injectable({
  providedIn: "root",
})
export class EmployeesServices {
  private readonly pathFilter = environment.endpoint.employeesControllerFilter;
  private readonly path = environment.endpoint.employeesController;
  public listSoggetti: any[] = [];

  constructor(private api: ApiService) {}


  public getAll(): Observable<any> {
    return this.api.get(this.path);
  }
  public getById(ut: any): Observable<any> {
    return this.api.getById(this.pathFilter , ut);
  }

  public getAllPage(primo:any , secondo:any): Observable<any> {
    return this.api.getByPage(this.path , primo , secondo);
  }

 
}
