
export const environment = {
    host: 'http://localhost:8090',
    endpoint: {
        teachersController: 'api/teachers',
        teachersControllerFilter: 'api/teachers/filter',
        employeesController: 'api/employees',
        employeesControllerFilter: 'api/employees/filter',
        courseController : 'api/corsi',
        courseControllerFilter : 'api/corsi/filter',
    }
}