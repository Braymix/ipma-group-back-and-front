CREATE DATABASE IF NOT EXISTS project_work;
USE project_work;

DROP TABLE IF EXISTS tipi;
CREATE TABLE tipi (
	tipo_id INTEGER AUTO_INCREMENT PRIMARY KEY,
	descrizione VARCHAR(50) NOT NULL
);

DROP TABLE IF EXISTS tipologie;
CREATE TABLE tipologie (
	tipologia_id INTEGER AUTO_INCREMENT PRIMARY KEY,
	descrizione VARCHAR(50) NOT NULL
);

DROP TABLE IF EXISTS misure;
CREATE TABLE misure (
	misura_id INTEGER AUTO_INCREMENT PRIMARY KEY,
	descrizione VARCHAR(50) NOT NULL
);

DROP TABLE IF EXISTS societa;
CREATE TABLE societa (
	societa_id INTEGER PRIMARY KEY AUTO_INCREMENT,
	ragione_sociale VARCHAR(60) NOT NULL,
	indirizzo VARCHAR(60),
	localita VARCHAR(60),
	provincia VARCHAR(60),
	nazione VARCHAR(60),
	telefono VARCHAR(60) NOT NULL,
	fax VARCHAR(60),
	partita_iva VARCHAR(11) NOT NULL
);

DROP TABLE IF EXISTS docenti;
CREATE TABLE docenti(
	docente_id INTEGER PRIMARY KEY AUTO_INCREMENT,
	titolo VARCHAR(60),
	ragione_sociale VARCHAR(60),
	indirizzo VARCHAR(60),
	localita VARCHAR(60),
	provincia VARCHAR(60),
	nazione VARCHAR(60),
	telefono VARCHAR(60) NOT NULL,
	fax VARCHAR(60),
	partita_iva VARCHAR(11),
	referente VARCHAR(60)
);

DROP TABLE IF EXISTS dipendenti;
CREATE TABLE dipendenti (
   dipendente_id INTEGER PRIMARY KEY AUTO_INCREMENT,
   cognome VARCHAR(70),
   nome VARCHAR(70),
   matricola INTEGER,
   societa INTEGER,
   sesso ENUM('maschio','femmina','preferisco non specificare'),
   data_nascita DATE,
   luogo_nascita VARCHAR(80),
   stato_civile VARCHAR(80),
   titolo_studio VARCHAR(100),
   conseguito_presso VARCHAR(80),
   anno_conseguimento INTEGER,
   tipo_dipendente VARCHAR(100),
   qualifica VARCHAR(100),
   livello VARCHAR(100),
   data_assunzione DATE,
   responsabile_risorsa INTEGER,
   responsabile_area INTEGER,
   data_fine_rapporto DATE,
   data_scadenza_contratto DATE,
    
   FOREIGN KEY (societa) REFERENCES societa(societa_id)
);

DROP TABLE IF EXISTS corsi;
CREATE TABLE corsi(
   corso_id INTEGER PRIMARY KEY AUTO_INCREMENT,
   tipo INT NOT NULL,
   tipologia INT NOT NULL,
   descrizione VARCHAR(200) NOT NULL,
   edizione VARCHAR(60) NOT NULL,
   previsto INT NOT NULL,
   erogato INT NOT NULL,
   durata INT NOT NULL,
	misura INT NOT NULL,
   note VARCHAR (60),
   luogo VARCHAR (60),
   ente VARCHAR (60),
   data_erogazione DATE,
   data_chiusura DATE,
   data_censimento DATE,
   interno INT,
   tipo_id INT,
   tipologia_id INT,
   misura_id INT,

   FOREIGN KEY (tipo) REFERENCES tipi(tipo_id),
   FOREIGN KEY (tipologia) REFERENCES tipologie(tipologia_id),
   FOREIGN KEY (misura) REFERENCES misure(misura_id)
);

DROP TABLE IF EXISTS valutazioni_utenti;
CREATE TABLE valutazioni_utenti(
   valutazioni_utenti_id INTEGER PRIMARY KEY AUTO_INCREMENT,
   corso INTEGER,
   dipendente INTEGER,
   criterio VARCHAR(100),
   valore VARCHAR(100),
    
   FOREIGN KEY (corso) REFERENCES corsi(corso_id),
   FOREIGN KEY (dipendente) REFERENCES dipendenti(dipendente_id)
);

DROP TABLE IF EXISTS corsi_docenti;
CREATE TABLE corsi_docenti (
	corsi_docenti_id INT AUTO_INCREMENT PRIMARY KEY,
	corso INT NOT NULL,
	docente INT NOT NULL,
	interno INT NOT NULL,
	
	FOREIGN KEY (corso) REFERENCES corsi(corso_id)
);

DROP TABLE IF EXISTS valutazioni_corsi;
CREATE TABLE valutazioni_corsi (
	valutazioni_corsi_id INT AUTO_INCREMENT PRIMARY KEY,
	corso INT NOT NULL ,
	criterio VARCHAR(100),
	valore VARCHAR(1),
	
	FOREIGN KEY (corso) REFERENCES corsi(corso_id)
);

DROP TABLE IF EXISTS valutazioni_docenti;
CREATE TABLE valutazioni_docenti(
   valutazioni_docenti_id INT PRIMARY KEY AUTO_INCREMENT,
   corso INT NOT NULL,
   docente INT NOT NULL,
   criterio VARCHAR(60),
   valore VARCHAR(60),

   FOREIGN KEY (corso) REFERENCES corsi(corso_id),
   FOREIGN KEY (docente) REFERENCES docenti(docente_id) 
);

DROP TABLE IF EXISTS corsi_utenti;
CREATE TABLE corsi_utenti(
	corsi_utenti_id INTEGER PRIMARY KEY AUTO_INCREMENT,
  	corso INT,
	dipendente INT,
 	durata VARCHAR(60),

  	FOREIGN KEY (corso) REFERENCES corsi(corso_id),
  	FOREIGN KEY (dipendente) REFERENCES dipendenti(dipendente_id)
);




INSERT INTO societa (societa_id, ragione_sociale, telefono, partita_iva) VALUES (null, 'Corvallis Spa', '0498434511','02070900283');
INSERT INTO dipendenti (matricola, societa, cognome, nome) VALUES (123, 1,'Lazic','Igor');
INSERT INTO dipendenti (matricola, societa, cognome, nome) VALUES (234, 1, 'Maraga','Manuel');
INSERT INTO dipendenti (matricola, societa, cognome, nome) VALUES (345, 1, 'Panarotto','Michele');
INSERT INTO dipendenti (matricola, societa, cognome, nome) VALUES (456, 1, 'Manuc','Adrian');

INSERT INTO docenti (titolo, indirizzo, telefono) VALUES ('Dr Martins', 'Via Europa 34','345963198');
INSERT INTO docenti (titolo, indirizzo, telefono) VALUES ('Ingegner Luigi','Via America 92','345956329');

INSERT INTO tipi VALUES (1,'Corso ITS');

INSERT INTO tipologie VALUES (1,'Corso informatico');

INSERT INTO misure VALUES (1,'Boh');

INSERT INTO corsi (tipo, tipologia, descrizione, edizione, previsto, erogato, durata, misura, tipo_id) VALUES (1, 1, 'Java', 'Boh', 1,1,1,1,1);
INSERT INTO corsi (tipo, tipologia, descrizione, edizione, previsto, erogato, durata, misura, tipo_id) VALUES (1, 1, 'Phyton', 'Boh', 2,1,1,1,1);
INSERT INTO corsi (tipo, tipologia, descrizione, edizione, previsto, erogato, durata, misura, tipo_id) VALUES (1, 1, 'Spring', 'Boh', 2,1,1,1,1);

INSERT INTO corsi_docenti VALUES (null, 1, 1, 1);

INSERT INTO corsi_utenti VALUES (null, 1, 1, 1);
